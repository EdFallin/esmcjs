let express = require("express");
let router = express.Router();

let service = null;

router.get("/:text", function (request, response, next) {
    lazyInitService()
        .then(x => {
            sendHttpResponse(request, response, next);
        })
        .catch(e => {
            console.log("catch:", e);
        })
    ;
});

async function lazyInitService() {
    // only init the service if it doesn't exist already
    if (service != null) {
        return;
    }

    // chain from a promise of a module, to a module,
    // to a class definition, to a class instance
    let holder = await import("../public/javascripts/ServiceClass.js");
    let definition = holder.ServiceClass;
    let instance = new definition();

    // retain the instance on the outer context
    service = instance;
}

function sendHttpResponse(request, response, next) {
    try {
        let text = request.params.text;
        text = service.reverse(text);
        response.send(text);
    }
    catch (e) {
        console.log("Thrown:", e);
    }
}

module.exports = router;

