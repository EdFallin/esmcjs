
let input = document.getElementById("Input");
let button = document.getElementById("Button");
let output = document.getElementById("Output");

button.addEventListener("click", roundTrip);

async function roundTrip(e) {
    let to = input.value;

    try {
        let from = await fetch("service/" + to);

        if (!from.ok) {
            throw Error("Fetch failed.");
        }

        let text = await from.text();
        output.textContent = text;
    }
    catch (e) {
        console.log(e);
    }
}
