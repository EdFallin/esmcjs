
/* exporting the definition of a class, not an instance of it */
export class ServiceClass {
    constructor() {
        /* no operations */
    }

    reverse(input) {
        let chars = Array.from(input);
        chars = chars.reverse();

        let output = chars.join("");
        return output;
    }
}

